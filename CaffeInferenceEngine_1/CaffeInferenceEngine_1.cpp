/**
*
* Copyright(c)<2018>, <Huawei Technologies Co.,Ltd>
*
* @version 1.0
*
* @date 2018-5-22
*/
#include <Python.h>
#include "CaffeInferenceEngine_1.h"
#include <hiaiengine/log.h>
#include <hiaiengine/ai_types.h>
#include <proto/graph_config.pb.h>
#include <vector>
#include <unistd.h>
#include <thread>
#include <fstream>
#include <algorithm>
#include <iostream>
#include <time.h>
#include <unistd.h>
#include <math.h>
#include <sys/wait.h>

const static int IMAGE_INFO_DATA_NUM = 3; //fasterrcnn image info data number
const static int LINE_LENGTH = 300;
const static int FILE_NAME_LENGTH = 32;

static void GetCurTime(char *buf, const int len)
{
    if (NULL == buf || len <= 1) {
        return;
    }
    time_t now;
    struct tm *tmNow = NULL;

    time(&now);
    tmNow = localtime(&now);
    errno_t err = snprintf_s(buf, len, len - 1, "%04d%02d%02d%02d%02d%02d", tmNow->tm_year+1900, tmNow->tm_mon+1, tmNow->tm_mday,
        tmNow->tm_hour, tmNow->tm_min, tmNow->tm_sec);
    if (0 > err) {
        HIAI_ENGINE_LOG(HIAI_IDE_ERROR, "sprintf_s failed, err: %d", err);
        return;
    }
}

//data of unsigned char type transfer to float type
static int ConvertU1ToF1(const unsigned char *src, float *des, const int size)
{
    if (NULL == src || NULL == des) {
        HIAI_ENGINE_LOG(HIAI_IDE_ERROR, "ERROR:input data is null.");
        return -1;
    }

    int i = 0;
    int j = 0;
    for(i = 0; i < size ; i += 4) {
        FloatData fd;
        fd.data[0] = src[i];
        fd.data[1] = src[i + 1];
        fd.data[2] = src[i + 2];
        fd.data[3] = src[i + 3];
        des[j++] = fd.f;
    }
    return 0;
}

static void FreeOutPutData(std::vector<std::shared_ptr<hiai::IAITensor>> &outputDataVec)
{
    for (std::shared_ptr<hiai::IAITensor> tensor: outputDataVec){
        std::shared_ptr<hiai::AISimpleTensor> resultTensor = std::static_pointer_cast<hiai::AISimpleTensor>(tensor);
        if (resultTensor != NULL && resultTensor->GetBuffer() != NULL) {
            free(resultTensor->GetBuffer());
        }
    }
    outputDataVec.clear();
}

// user fopen to execute python scripts. Main procedure will be waiting until the command runs to the end.
static bool CommandExe(const char *cmd)
{
    if (nullptr == cmd) {
        HIAI_ENGINE_LOG(HIAI_IDE_ERROR, "cmd param error.");
        return false;
    }

    FILE *file = nullptr;
    char line[LINE_LENGTH] = {0};
    file = popen(cmd, "r"); // popen func need safe check "cmd" param before use it
    if (nullptr != file) {
        while (fgets(line, LINE_LENGTH, file) != NULL) {
            ;
        }
    } else {
        HIAI_ENGINE_LOG(HIAI_IDE_ERROR, "popen error.");
        return false;
    }
    pclose(file);
    return true;
}

HIAI_StatusT CaffeInferenceEngine_1::Init(const hiai::AIConfig& config,
   const std::vector<hiai::AIModelDescription>& modelDesc)
{
    int ret = memset_s(&caffeDumpName_, sizeof(caffeDumpName_), 0, sizeof(caffeDumpName_));
    if (0 != ret) {
        HIAI_ENGINE_LOG(HIAI_IDE_ERROR, "[CaffeInferenceEngine_1] memset_s error.");
        return HIAI_ERROR;
    }

    for (int index = 0; index < config.items_size(); ++index) {
        const ::hiai::AIConfigItem& item = config.items(index);
        if (item.name() == "model_file") {
            caffeCfgInfo_.modelPath_ = item.value().data();
        } else if (item.name() == "weight_file") {
            caffeCfgInfo_.weightPath_ = item.value().data();
        }
    }
    GetCurTime(caffeDumpName_, sizeof(caffeDumpName_));

    //get oam config for caffe dump layer list
    std::string sepChar(",");
    hiai::OAMConfigDef *oamConfig = GetOamConfig();
    if (nullptr != oamConfig) {
        for (unsigned index = 0; index < oamConfig->dump_list_size(); index++) {
            hiai::DumpDef dumpList = oamConfig->dump_list(index);
            if (dumpList.is_dump_all() == "true") {
                caffeCfgInfo_.dumpList_ = "all";
            } else {
                for (unsigned layerIndex = 0; layerIndex < dumpList.layer_size() && !dumpList.layer(layerIndex).empty(); layerIndex++) {
                    caffeCfgInfo_.dumpList_ += dumpList.layer(layerIndex) + sepChar;
                }
            }
        }
        caffeCfgInfo_.dumpPath_ = oamConfig->dump_path();
    }
    if (!caffeCfgInfo_.dumpList_.empty() && caffeCfgInfo_.dumpList_ != "all") { // delete last char "," in caffeCfgInfo_.dumpList_
        size_t lastCharPos = caffeCfgInfo_.dumpList_.find_last_of(sepChar);
        if (std::string::npos == lastCharPos || lastCharPos <= 1) {
            HIAI_ENGINE_LOG(HIAI_IDE_ERROR, "ERROR:find last char pos error.");
            return HIAI_ERROR;
        }
        caffeCfgInfo_.dumpList_ = caffeCfgInfo_.dumpList_.substr(0, lastCharPos);
    }

    PyGILState_STATE state = PyGILState_Ensure(); //get GIL before call python api, caffe may call layer implemented by python
    Caffe::set_mode(Caffe::CPU);
    Net<float> *net = nullptr;
    try {
        net = new Net<float>(caffeCfgInfo_.modelPath_, TEST);
    }
    catch (const std::bad_alloc& e) {
        HIAI_ENGINE_LOG(HIAI_IDE_ERROR, "[CaffeInferenceEngine_1] net alloc fails.");
        return HIAI_ERROR;
    }
    net_.reset(net);
    net_->CopyTrainedLayersFrom(caffeCfgInfo_.weightPath_);
    PyGILState_Release(state); //release GIL after call python api

    if (net_->has_blob("data") && NULL != net_->blob_by_name("data")) {
        batchSize_ = net_->blob_by_name("data")->num();
    }

    return HIAI_OK;
}

/**
* @brief: handle the exceptions
* @in: errorMsg: the error message
*/
void CaffeInferenceEngine_1::HandleExceptions(const std::string errorMsg)
{
    HIAI_ENGINE_LOG(HIAI_IDE_ERROR, errorMsg.c_str());
    if (NULL == tranData_ || NULL == tranData_.get()) {
        HIAI_ENGINE_LOG(HIAI_IDE_ERROR, "[CaffeInferenceEngine_1] tranData_ is null!");
        return;
    }
    tranData_->status = false;
    tranData_->msg = errorMsg;
    //send null to next node to avoid blocking when to encounter abnomal situation.
    auto ret = SendData(0, "EngineTransT", std::static_pointer_cast<void>(tranData_));
    if (ret != HIAI_OK) {
        HIAI_ENGINE_LOG(HIAI_IDE_ERROR, "[CaffeInferenceEngine_1] send data failed!");
    }
}

/**
* @brief: send sentinel image to inform the graph to destroy
*/
HIAI_StatusT CaffeInferenceEngine_1::SendSentinelImage()
{
    if (NULL == tranData_ || NULL == tranData_.get()) {
        tranData_ = std::make_shared<EngineTransT>();
        if (NULL == tranData_ || NULL == tranData_.get()) {
            HandleExceptions("[CaffeInferenceEngine_1] tranData_ is nullptr");
            return HIAI_ERROR;
        }
    }
    if (NULL == imageHandle_ || NULL == imageHandle_.get()) {
        HandleExceptions("[CaffeInferenceEngine_1] imageHandle_ is nullptr");
        return HIAI_ERROR;
    }

    tranData_->status = true;
    tranData_->msg = "sentinel Image";
    tranData_->b_info = imageHandle_->b_info;
    HIAI_StatusT hiaiRet = HIAI_OK;
    do {
        HIAI_ENGINE_LOG(HIAI_IDE_INFO, "[CaffeInferenceEngine_1] sentinel image, process success!");
        hiaiRet = SendData(0, "EngineTransT", std::static_pointer_cast<void>(tranData_));
        if (HIAI_OK != hiaiRet) {
            if (HIAI_ENGINE_NULL_POINTER == hiaiRet || HIAI_HDC_SEND_MSG_ERROR == hiaiRet || HIAI_HDC_SEND_ERROR == hiaiRet
                    || HIAI_GRAPH_SRC_PORT_NOT_EXIST == hiaiRet || HIAI_GRAPH_ENGINE_NOT_EXIST == hiaiRet) {
                HIAI_ENGINE_LOG(HIAI_IDE_ERROR, "[CaffeInferenceEngine_1] SendData error[%d], break.", hiaiRet);
                break;
            }
            HIAI_ENGINE_LOG(HIAI_IDE_INFO, "[CaffeInferenceEngine_1] SendData return value[%d] not OK, sleep 200ms", hiaiRet);
            usleep(SEND_DATA_INTERVAL_MS);
        }
    } while (HIAI_OK != hiaiRet);
    return hiaiRet;
}

/**
* @brief: PreProcess arg0, especially for multi batch scene
*/
void CaffeInferenceEngine_1::PrepareData(const std::shared_ptr<void> &data)
{
    if (nullptr != data) {
        std::shared_ptr<BatchImageParaWithScaleT> dataInput = std::static_pointer_cast<BatchImageParaWithScaleT>(data);
        if (!isSentinelImage(dataInput)) {
            if (dataInput_ != nullptr) {
                if (dataInput_->b_info.batch_ID == dataInput->b_info.batch_ID && !dataInput->v_img.empty() && !dataInput->b_info.frame_ID.empty()) {
                    dataInput_->v_img.push_back(dataInput->v_img[0]);
                    dataInput_->b_info.frame_ID.push_back(dataInput->b_info.frame_ID[0]);
                }
            } else {
                dataInput_ = dataInput;
            }
            if (dataInput_->v_img.size() != dataInput_->b_info.batch_size) {
                HIAI_ENGINE_LOG(HIAI_IDE_INFO, "[CaffeInferenceEngine_1] Wait for other %d batch image info!",
                    (dataInput_->b_info.batch_size - dataInput_->v_img.size()));
                return;
            }
            inputQue_.PushData(0, dataInput_);
            dataInput_ = nullptr;
        } else {
            inputQue_.PushData(0, data);
        }
    }
}

/**
* @brief: prepare the data buffer for image information
* @in: inputBuffer: buffer pointer
* @in: imageNumber: total number of received images
* @in: batchBegin: the index of the first image of each batch
* @in: multiInput: the second input received from the previous engine
* @return: HIAI_StatusT
*/
HIAI_StatusT CaffeInferenceEngine_1::PrepareInfoInput(uint8_t *inputBuffer, const int batchBegin, const std::shared_ptr<hiai::BatchRawDataBuffer> &multiInput)
{
    if (NULL == inputBuffer || NULL == multiInput || NULL == multiInput.get()) {
        HIAI_ENGINE_LOG(HIAI_IDE_ERROR, "[CaffeInferenceEngine_1] PrepareInputBuffer param error.");
        return HIAI_ERROR;
    }

    int eachSize = 0;
    //the loop for each info
    for (int j = 0; j < imageHandle_->b_info.batch_size; j++) {
        if (batchBegin + j < imageHandle_->v_img.size()) {
            hiai::RawDataBuffer inputArg = multiInput->v_info[batchBegin + j];
            eachSize = inputArg.len_of_byte;
            HIAI_ENGINE_LOG(HIAI_IDE_INFO, "[CaffeInferenceEngine_1] info each input size: %d", eachSize);
            if (memcpy_s(inputBuffer + j * eachSize, eachSize, inputArg.data.get(), eachSize)) {
                HIAI_ENGINE_LOG(HIAI_IDE_ERROR, "[CaffeInferenceEngine_1] ERROR, copy info buffer failed");
                return HIAI_ERROR;
            }
        } else {
            float infoTmp[3] = { 0.0, 0.0, 0.0 };
            eachSize = sizeof(infoTmp);
            HIAI_ENGINE_LOG(HIAI_IDE_INFO, "[CaffeInferenceEngine_1] info padding size: %d", eachSize);
            if (memcpy_s(inputBuffer + j * eachSize, eachSize, infoTmp, eachSize)) {
                HIAI_ENGINE_LOG(HIAI_IDE_ERROR, "[CaffeInferenceEngine_1] ERROR, padding info buffer failed");
                return HIAI_ERROR;
            }
        }
    }
    return HIAI_OK;
}

/**
* @brief: set the frame ID as -1 to indicate this model batch failed
* @in: index of the begin of this batch
*/
void CaffeInferenceEngine_1::HandleModelBatchFailure(const int batchBegin)
{
    if (NULL == tranData_ || NULL == tranData_.get() || NULL == imageHandle_ || NULL == imageHandle_.get()) {
        HIAI_ENGINE_LOG(HIAI_IDE_ERROR, "[CaffeInferenceEngine_1] imageHandle_ is null!");
        return;
    }
    for (int i = 0; i < imageHandle_->b_info.batch_size; i++) {
        tranData_->b_info.frame_ID[i + batchBegin] = -1;
    }
}

//process caffe inference result, and then send data to next engine.
HIAI_StatusT CaffeInferenceEngine_1::ProcessCaffeResult(std::vector<std::shared_ptr<hiai::IAITensor>> &outputDataVec)
{
    if (NULL == tranData_ || NULL == tranData_.get()) {
        FreeOutPutData(outputDataVec);
        tranData_ = std::make_shared<EngineTransT>();
        if (NULL == tranData_ || NULL == tranData_.get()) {
            HandleExceptions("[CaffeInferenceEngine_1] tranData_ is nullptr");
            return HIAI_ERROR;
        }
    }

    if (batchSize_ == 0) {
        FreeOutPutData(outputDataVec);
        HIAI_ENGINE_LOG(HIAI_IDE_ERROR, "[CaffeInferenceEngine_1] batchSize_ is 0, ERROR.");
        return HIAI_ERROR;
    }
    int cntBatch = imageHandle_->b_info.batch_size / batchSize_; // b_info.batch_size refers to dataset's batch and batchSize_ refers to model
    if (imageHandle_->b_info.batch_size % batchSize_ != 0) {
        cntBatch++;
    }

    tranData_->b_info = imageHandle_->b_info;
    tranData_->b_info.max_batch_size = cntBatch * batchSize_; // max_batch_size include invalid data
    tranData_->size = outputDataVec.size() / cntBatch; // get size of each batch
    tranData_->v_img = imageHandle_->v_img;
    tranData_->status = true;
    tranData_->msg = "CaffeInferenceEngine_1"; // notify next node that this is the result of caffe inference engine.

    std::map<std::string, u_int8_t*> totalDataMap; // the whole data of multi-batch and multi-output. Seperate by each output's name.
    std::map<std::string, u_int8_t*> headPointerMap; // store the header address of every output.
    std::map<std::string, OutputT> outMap;
    for (int i = 0; i < outputDataVec.size(); ++i) {
        OutputT out = {0};
        std::shared_ptr<hiai::AISimpleTensor> resultTensor = std::static_pointer_cast<hiai::AISimpleTensor>(outputDataVec[i]);
        if (NULL == resultTensor || NULL == resultTensor.get()) {
            HIAI_ENGINE_LOG(HIAI_IDE_ERROR, "[CaffeInferenceEngine_1]img.data is null, ERROR.");
            FreeOutPutData(outputDataVec);
            return HIAI_ERROR;
        }

        out.size = resultTensor->GetSize() * cntBatch;
        out.name = std::static_pointer_cast<hiai::AINeuralNetworkBuffer>(resultTensor)->GetName();
        HIAI_ENGINE_LOG(HIAI_IDE_INFO, "[CaffeInferenceEngine_1] out[%d] cntBatch:%d, size:%d, name:%s.", i, cntBatch, out.size, out.name.c_str());

        // If's we can't find out.name, store the float data in the name with new buffer.
        // If we can find out.name, store the float data at the end of totalDataMap[out.name]
        if (totalDataMap.find(out.name) == totalDataMap.end()) {
            try {
                totalDataMap[out.name] = new u_int8_t[out.size];
            }
            catch (const std::bad_alloc& e) {
                HIAI_ENGINE_LOG(HIAI_IDE_ERROR, "[CaffeInferenceEngine_1] ptr alloc fails.");
                FreeOutPutData(outputDataVec);
                return HIAI_ERROR;
            }
            if (memset_s(totalDataMap[out.name], out.size, static_cast<char>(0), out.size) != 0) {
                HIAI_ENGINE_LOG(HIAI_IDE_ERROR, "[CaffeInferenceEngine_1] ptr init fails");
                FreeOutPutData(outputDataVec);
                delete totalDataMap[out.name];
                totalDataMap[out.name] = NULL;
                return HIAI_ERROR;
            }
            headPointerMap[out.name] = totalDataMap[out.name]; // record the head address of buffer
            outMap[out.name] = out;
        }

        // get current address of totalDataMap[out.name]
        u_int8_t* ptr = totalDataMap[out.name];
        float *pf = (float *)resultTensor->GetBuffer();
        int j = 0;
        for (int k = 0; k < resultTensor->GetSize() / sizeof(float); k++) {
            FloatData fd;
            fd.f = pf[k];
            ptr[j++] = fd.data[0];
            ptr[j++] = fd.data[1];
            ptr[j++] = fd.data[2];
            ptr[j++] = fd.data[3];
        }
        // move the pointer
        totalDataMap[out.name] += j;
    }

    if (outMap.size() == 0) {
        HIAI_ENGINE_LOG(HIAI_IDE_ERROR, "[CaffeInferenceEngine_1] output is empty");
        FreeOutPutData(outputDataVec);
        return HIAI_ERROR;
    }

    // push_back all header address of data
    std::map<std::string, OutputT>::iterator iter;
    for (iter = outMap.begin(); iter != outMap.end(); iter++) {
        iter->second.data.reset(headPointerMap[iter->first]);
        tranData_->output_data_vec.push_back(iter->second);
    }

    HIAI_StatusT hiaiRet = HIAI_OK;
    do {
        hiaiRet = SendData(0, "EngineTransT", std::static_pointer_cast<void>(tranData_));
        if (HIAI_QUEUE_FULL == hiaiRet) {
            HIAI_ENGINE_LOG(HIAI_IDE_INFO, "[CaffeInferenceEngine_1] queue full, sleep 200ms");
            usleep(SEND_DATA_INTERVAL_MS);
        }
    } while (hiaiRet == HIAI_QUEUE_FULL);
    if (HIAI_OK != hiaiRet) {
        HIAI_ENGINE_LOG(HIAI_IDE_ERROR, "[CaffeInferenceEngine_1] SendData failed! error code: %d", hiaiRet);
        return HIAI_ERROR;
    }

    return HIAI_OK;
}

// dump caffe layer data, only 3 inputs for dump data in default, the last two inputs is for INPUT_SIZE = 3
void CaffeInferenceEngine_1::CaffeDumpProcess(const uint32_t imageFrameID, const uint8_t *buffer1, const int buffer1Size,
    const uint8_t *buffer2/* = nullptr*/, const int buffer2Size/* = 0*/)
{
    // dump the first data which is first input layer of net generally
    if (nullptr == buffer1 || buffer1Size < 0) {
        HIAI_ENGINE_LOG(HIAI_IDE_ERROR, "[CaffeInferenceEngine_1] CaffeDumpProcess param error.");
        return;
    }
    char pillowDataFileName[FILE_NAME_LENGTH] = {0};
    errno_t err = snprintf_s(pillowDataFileName, sizeof(pillowDataFileName), sizeof(pillowDataFileName) - 1, "%d%s", imageFrameID, "_pillowData.bin");
    if (0 > err) {
        HIAI_ENGINE_LOG(HIAI_IDE_ERROR, "sprintf_s failed, err: %d", err);
        return;
    }
    if (!StorageCaffeData(buffer1, buffer1Size, imageFrameID, pillowDataFileName)) {
        HIAI_ENGINE_LOG(HIAI_IDE_ERROR, "StorageCaffeData pillow data error");
        return;
    }

    // dump the second input data, for example, img_info for fasterrcnn
    std::string imgInfo = "";
    if (nullptr != buffer2 && buffer2Size > 0)
    {
        char imgInfoFileName[FILE_NAME_LENGTH] = {0};
        errno_t err = snprintf_s(imgInfoFileName, sizeof(imgInfoFileName), sizeof(imgInfoFileName) - 1, "%d%s", imageFrameID, "_imgInfo.bin");
        if (0 > err) {
            HIAI_ENGINE_LOG(HIAI_IDE_ERROR, "sprintf_s failed, err: %d", err);
            return;
        }
        if (!StorageCaffeData(buffer2, buffer2Size, imageFrameID, imgInfoFileName)) {
            HIAI_ENGINE_LOG(HIAI_IDE_ERROR, "StorageCaffeData img info data error");
            return;
        }
        imgInfo = std::string(imgInfoFileName);
    }

    // dump data from caffe inference
    DumpCaffeLayer(imageFrameID, std::string(pillowDataFileName), imgInfo);
}

//storage data for py call to dump layer
bool CaffeInferenceEngine_1::StorageCaffeData(const unsigned char *inputData, const uint32_t size, const uint32_t imageFrameID, const char *fileName)
{
    if (nullptr == inputData || nullptr == fileName) {
        HIAI_ENGINE_LOG(HIAI_IDE_ERROR, "ERROR:inputData/fileName is null.");
        return false;
    }
    FILE *fd = fopen(fileName, "w+"); //file mode:644
    if (nullptr != fd) {
        HIAI_ENGINE_LOG(HIAI_IDE_INFO, "fileName:%s.", fileName);
        chmod(fileName, S_IRUSR | S_IWUSR); //chmod file mode:600
        fwrite(inputData, 1, size, fd);
        fclose(fd);
    } else {
        HIAI_ENGINE_LOG(HIAI_IDE_ERROR, "fd is null, cannot write data to file.");
        return false;
    }
    return true;
}

//add caffe dump function for dumcp caffe layer data
void CaffeInferenceEngine_1::DumpCaffeLayer(const uint32_t imageFrameID, const std::string file, const std::string imgInfoFile/* = ""*/)
{
    static uint32_t index = 0;
    //1. transfer prototxt file for dump every layer.
    if (caffeCfgInfo_.dumpPath_.empty() || caffeCfgInfo_.dumpList_.empty()) {
        HIAI_ENGINE_LOG(HIAI_IDE_ERROR, "ERROR:get dumpPath_ error, cannot dump caffe layer.");
        return;
    }

    // get installation path from parse dumpPath_
    // for example, dump_path: "/mnt/mind/tools/che/dump"
    std::string findString("/che/dump");
    size_t lastCharPos = caffeCfgInfo_.dumpPath_.find_last_of(findString);
    if (std::string::npos == lastCharPos || lastCharPos <= 1) {
        HIAI_ENGINE_LOG(HIAI_IDE_ERROR, "ERROR:find last char pos error.");
        return;
    }
    std::string installationPath = caffeCfgInfo_.dumpPath_.substr(0, lastCharPos - findString.length() + 1);
    if ( -1 == access(installationPath.c_str(), 0)) { //check whether installation path is correct
        HIAI_ENGINE_LOG(HIAI_IDE_ERROR, "installationPath(%s) does not exist", installationPath.c_str());
        return;
    }

    std::string scriptPath = installationPath + "/che/ddk/ddk/toolchains/operator_cmp/";
    std::string transferPy = scriptPath + "golden_gen/caffe/ProcInplaceLayer.pyc";
    if (-1 == access(transferPy.c_str(), 0)) { //equals to white list check by assure whether file exists
        HIAI_ENGINE_LOG(HIAI_IDE_ERROR, "transferPy(%s) does not exist", transferPy.c_str());
        return;
    }
    std::string newProtoTxt = caffeCfgInfo_.modelPath_ + std::string("_new");
    std::string cmdTransferPy = "python " + transferPy + " " + caffeCfgInfo_.modelPath_ + " " + newProtoTxt;
    HIAI_ENGINE_LOG(HIAI_IDE_INFO, "cmdTransfer: %s", cmdTransferPy.c_str());
    if (!CommandExe(cmdTransferPy.c_str())) {
        HIAI_ENGINE_LOG(HIAI_IDE_ERROR, "cmdTransferPy executes error.");
        return;
    }

    //2. dump caffe layer according to dumpList_, outputDir follows ome dump named like this:installationPath/che/dump/20181010192849_caffe/
    std::string outputDir = caffeCfgInfo_.dumpPath_ + "/" + std::string(caffeDumpName_) + "_caffe";
    std::string inferencePy = scriptPath + "net_inference/caffe/inference.pyc";
    if (-1 == access(inferencePy.c_str(), 0)) { //equals to white list check by assure whether file exists
        HIAI_ENGINE_LOG(HIAI_IDE_ERROR, "inferencePy(%s) does not exist", inferencePy.c_str());
        return;
    }

    std::string cmdInference = "python " + scriptPath + "net_inference/caffe/inference.pyc -pt " + newProtoTxt + " -list " + caffeCfgInfo_.dumpList_ \
        + " -model " + caffeCfgInfo_.weightPath_ + " -o " + outputDir + " -data " + file + " -id " + std::to_string(index);
    if (!imgInfoFile.empty()) {
        cmdInference += " -im_info " + imgInfoFile;
    }
    HIAI_ENGINE_LOG(HIAI_IDE_INFO, "cmdInference: %s", cmdInference.c_str());
    if (!CommandExe(cmdInference.c_str())) {
        HIAI_ENGINE_LOG(HIAI_IDE_ERROR, "cmdInference executes error.");
        return;
    }

    index++;
}

//combine data for inference according to batch size
HIAI_StatusT CaffeInferenceEngine_1::PrepareInputBuffer(uint8_t *inputBuffer, const int imageNumber, const int batchBegin, const int imageSize)
{
    if (nullptr == inputBuffer || nullptr == imageHandle_ || nullptr == imageHandle_.get()
        || imageNumber < 0 || batchBegin < 0 || imageSize < 0) {
        HIAI_ENGINE_LOG(HIAI_IDE_ERROR, "[CaffeInferenceEngine_1] PrepareInputBuffer param error.");
        return HIAI_ERROR;
    }
    for (int j = 0; j < batchSize_; j++) {
        if (batchBegin + j < imageNumber) {
            if (0 != memcpy_s(inputBuffer + j * imageSize, imageSize, imageHandle_->v_img[batchBegin + j].img.data.get(), imageSize)) {
                HIAI_ENGINE_LOG(HIAI_IDE_ERROR, "[CaffeInferenceEngine_1] ERROR, copy image buffer failed");
                return HIAI_ERROR;
            }
        } else {
            if (0 != memset_s(inputBuffer + j * imageSize, imageSize, static_cast<char>(0), imageSize)) {
                HIAI_ENGINE_LOG(HIAI_IDE_ERROR, "[CaffeInferenceEngine_1] ERROR, batch padding for image data failed");
                return HIAI_ERROR;
            }
        }
    }
    return HIAI_OK;
}

//chech caffe param, compare data in prototxt with user's setting
HIAI_StatusT CaffeInferenceEngine_1::CheckCaffeParam()
{
    if (NULL == imageHandle_ || NULL == imageHandle_.get()) {
        HIAI_ENGINE_LOG(HIAI_IDE_ERROR, "[CaffeInferenceEngine_1] CheckCaffeParam param error.");
        return HIAI_ERROR;
    }

    if (net_->has_blob("data") && NULL != net_->blob_by_name("data")) {
        //compare width/height in prototxt with resize_width/resize_height, if not match, return error.
        int heightProto = net_->blob_by_name("data")->height();
        int widthProto = net_->blob_by_name("data")->width();
        uint32_t imgHeight = imageHandle_->v_img[0].img.height;
        uint32_t imgWidth = imageHandle_->v_img[0].img.width;
        if (heightProto != imgHeight || widthProto != imgWidth) {
            HIAI_ENGINE_LOG(HIAI_IDE_ERROR, "[CaffeInferenceEngine_1] prototxt param width[%d]/height[%d] donot match img width[%d]/height[%d].",
                widthProto, heightProto, imgWidth, imgHeight);
            return HIAI_ERROR;
        }
    }
    return HIAI_OK;
}

HIAI_IMPL_ENGINE_PROCESS("CaffeInferenceEngine_1", CaffeInferenceEngine_1, INPUT_SIZE)
{
    HIAI_ENGINE_LOG(HIAI_IDE_INFO, "[CaffeInferenceEngine_1] start process!");
    HIAI_StatusT hiaiRet = HIAI_OK;
    uint32_t nnInputSize = 0;
    std::vector<std::shared_ptr<hiai::IAITensor>> outputDataVec;
    imageHandle_ = nullptr;
    inputDataVec_.clear();
    tranData_ = std::make_shared<EngineTransT>();
    if (NULL == tranData_ || NULL == tranData_.get()) {
        HandleExceptions("[CaffeInferenceEngine_1_1] tranData_ is nullptr");
        return HIAI_ERROR;
    }

    //1.0 get port 0 data(arg0) to imageHandle_, and portN data(if exist(arg2...), N > 1) to _multi_input_N
    PrepareData(arg0); //process port0 data arg0, if multi batch, get all batch_size data.
#if INPUT_SIZE < 3
    if (!inputQue_.PopAllData(imageHandle_)) {
        HandleExceptions("[CaffeInferenceEngine_1] fail to PopAllData");
        return HIAI_ERROR;
    }
#endif

#if (INPUT_SIZE == 3)
    DEFINE_MULTI_INPUT_ARGS_POP(3);
#endif

#if (INPUT_SIZE == 4)
    DEFINE_MULTI_INPUT_ARGS_POP(4);
#endif

#if (INPUT_SIZE == 5)
    DEFINE_MULTI_INPUT_ARGS_POP(5);
#endif

#if (INPUT_SIZE == 6)
    DEFINE_MULTI_INPUT_ARGS_POP(6);
#endif

#if (INPUT_SIZE == 7)
    DEFINE_MULTI_INPUT_ARGS_POP(7);
#endif

#if (INPUT_SIZE == 8)
    DEFINE_MULTI_INPUT_ARGS_POP(8);
#endif

#if (INPUT_SIZE == 9)
    DEFINE_MULTI_INPUT_ARGS_POP(9);
#endif

#if (INPUT_SIZE == 10)
    DEFINE_MULTI_INPUT_ARGS_POP(10);
#endif

    if (imageHandle_ == nullptr) {
        HandleExceptions("[CaffeInferenceEngine_1] imageHandle_ is nullptr");
        return HIAI_ERROR;
    }

    //2.0 check sentinel image which means that all data in dataset are all sended, this is last step.
    if (isSentinelImage(imageHandle_)) {
        return SendSentinelImage();
    }

    //3.0 check caffe param setted by user
    if (CheckCaffeParam() != HIAI_OK) {
        HandleExceptions("[CaffeInferenceEngine_1] fail to process user's invalid caffe param.");
        return HIAI_ERROR;
    }

    if (batchSize_ == 0) {
        HIAI_ENGINE_LOG(HIAI_IDE_ERROR, "[CaffeInferenceEngine_1] batchSize_ is 0, process ERROR.");
        return HIAI_ERROR;
    }

    if (imageHandle_->b_info.batch_size > batchSize_) {
        HIAI_ENGINE_LOG(HIAI_IDE_WARNING, "[CaffeInferenceEngine_1] Dataset's batch size should be less than model's batch size.");
    }

    int cntBatch = imageHandle_->b_info.batch_size / batchSize_;
    if (imageHandle_->b_info.batch_size % batchSize_ != 0) {
        cntBatch++;
    }

    //4.0 malloc memory for _input_bufferN, if port numbers user defined are larger than 3, user need to add code process portN(N>3)
    int imageSize = imageHandle_->v_img[0].img.size * sizeof(uint8_t);
    int imageNumber = imageHandle_->v_img.size();
    int inputBuffer1Size = imageSize * batchSize_;
    uint8_t *inputBuffer1 = NULL;
    try {
        inputBuffer1 = new uint8_t[inputBuffer1Size];
    }
    catch (const std::bad_alloc& e) {
        HIAI_ENGINE_LOG(HIAI_IDE_ERROR, "[CaffeInferenceEngine_1] failed to allocate buffer for first input!");
        return HIAI_ERROR;
    }
    if (memset_s(inputBuffer1, inputBuffer1Size, static_cast<char>(0), inputBuffer1Size) != 0) {
        HIAI_ENGINE_LOG(HIAI_IDE_ERROR, "[CaffeInferenceEngine_1] inputBuffer1 init fails");
        return HIAI_ERROR;
    }
    HIAI_ENGINE_LOG(HIAI_IDE_INFO, "[CaffeInferenceEngine_1] batch size:%d, img batch size:%d, cntBatch:%d, imageSize:%d.",
        batchSize_, imageHandle_->b_info.batch_size, cntBatch, imageSize);

#if (INPUT_SIZE == 3) //port 2
    if (nullptr == multiInput2) {
        if (nullptr != inputBuffer1) {
            delete [] inputBuffer1;
            inputBuffer1 = nullptr;
        }
        HandleExceptions("[CaffeInferenceEngine_1] multiInput2 is null.");
        return HIAI_ERROR;
    }
    int info_number = multiInput2->v_info.size();
    if (info_number != imageNumber) {
        if (nullptr != inputBuffer1) {
            delete [] inputBuffer1;
            inputBuffer1 = nullptr;
        }
        HandleExceptions("[CaffeInferenceEngine_1] the number of image data and info data doesn't match!");
        return HIAI_ERROR;
    }
    int inputBuffer2Size = sizeof(float) * IMAGE_INFO_DATA_NUM * batchSize_;
    uint8_t * inputBuffer2 = nullptr;
    try {
        inputBuffer2 = new uint8_t[inputBuffer2Size];
    }
    catch (const std::bad_alloc& e) {
        if (nullptr != inputBuffer1) {
            delete [] inputBuffer1;
            inputBuffer1 = nullptr;
        }
        HandleExceptions("[CaffeInferenceEngine_1] failed to allocate buffer for second input!");
        return HIAI_ERROR;
    }
    if (memset_s(inputBuffer2, inputBuffer2Size, static_cast<char>(0), inputBuffer2Size) != 0) {
        HIAI_ENGINE_LOG(HIAI_IDE_ERROR, "[CaffeInferenceEngine_1] inputBuffer2 init fails");
        return HIAI_ERROR;
    }
#endif

    //lambda function for release _input_bufferX
    auto releaseInputBuffer = [&] () { //[&] means use all var in this function
#if (INPUT_SIZE == 2)
        RELEASE_MULTI_INPUT_BUFFERS(2);
#endif

#if (INPUT_SIZE == 3)
        RELEASE_MULTI_INPUT_BUFFERS(3);
#endif

#if (INPUT_SIZE == 4)
        RELEASE_MULTI_INPUT_BUFFERS(4);
#endif

#if (INPUT_SIZE == 5)
        RELEASE_MULTI_INPUT_BUFFERS(5);
#endif

#if (INPUT_SIZE == 6)
        RELEASE_MULTI_INPUT_BUFFERS(6);
#endif

#if (INPUT_SIZE == 7)
        RELEASE_MULTI_INPUT_BUFFERS(7);
#endif

#if (INPUT_SIZE == 8)
        RELEASE_MULTI_INPUT_BUFFERS(8);
#endif

#if (INPUT_SIZE == 9)
        RELEASE_MULTI_INPUT_BUFFERS(9);
#endif

#if (INPUT_SIZE == 10)
        RELEASE_MULTI_INPUT_BUFFERS(10);
#endif
    };

    //lambda function for multi input set cpu data:net_->input_blobs()[index - 1]->set_cpu_data((float *)_input_buffer##index)
    auto setCPUData = [&] () { //[&] means use all var in this function
#if (INPUT_SIZE == 3)
        CAFFE_SET_CPU_DATAS(2);
#endif

#if (INPUT_SIZE == 4)
        CAFFE_SET_CPU_DATAS(3);
#endif

#if (INPUT_SIZE == 5)
        CAFFE_SET_CPU_DATAS(4);
#endif

#if (INPUT_SIZE == 6)
        CAFFE_SET_CPU_DATAS(5);
#endif

#if (INPUT_SIZE == 7)
        CAFFE_SET_CPU_DATAS(6);
#endif

#if (INPUT_SIZE == 8)
        CAFFE_SET_CPU_DATAS(7);
#endif

#if (INPUT_SIZE == 9)
        CAFFE_SET_CPU_DATAS(8);
#endif

#if (INPUT_SIZE == 10)
        CAFFE_SET_CPU_DATAS(9);
#endif
    };

    //5.0 caffe foward for each batch
    for(int imageId = 0; imageId < imageNumber; imageId += batchSize_) {
        //input data(port 0 for input_blobs[0]) process for inference
        if (HIAI_OK != PrepareInputBuffer(inputBuffer1, imageNumber, imageId, imageSize)) {
            HIAI_ENGINE_LOG(HIAI_IDE_ERROR, "[CaffeInferenceEngine_1] failed to PrepareInputBuffer!");
            HandleModelBatchFailure(imageId);
            releaseInputBuffer();
            return HIAI_ERROR;
        }

        nnInputSize = imageSize * batchSize_;
        HIAI_ENGINE_LOG(HIAI_IDE_INFO, "[CaffeInferenceEngine_1] nnInputSize:%d, width:%d, height:%d",
            nnInputSize, imageHandle_->v_img[imageId].img.width, imageHandle_->v_img[imageId].img.height);
        if (nnInputSize > 0) {
#if (INPUT_SIZE == 2)
            DEFINE_MULTI_INPUT_ARGS(2);
#endif
#if (INPUT_SIZE == 3)
            if (HIAI_OK != PrepareInfoInput(inputBuffer2, imageId, multiInput2)) {
                HandleModelBatchFailure(imageId);
                continue;
            }
            DEFINE_MULTI_INPUT_ARGS(3);
#endif

            if (!caffeCfgInfo_.dumpList_.empty()) { // caffe dump process
#if (INPUT_SIZE == 2)
                CaffeDumpProcess(imageHandle_->b_info.frame_ID[imageId], inputBuffer1, inputBuffer1Size);
#elif (INPUT_SIZE == 3)
                CaffeDumpProcess(imageHandle_->b_info.frame_ID[imageId], inputBuffer1, inputBuffer1Size, inputBuffer2, inputBuffer2Size);
#endif
            }
            HIAI_ENGINE_LOG(HIAI_IDE_INFO, "[CaffeInferenceEngine_1] img size:%d, caffe num_inputs:%d.", imageHandle_->v_img[imageId].img.size, net_->num_inputs());
            float *outputData = nullptr;
            try {
                outputData = new float[(int)ceil(1.0 * nnInputSize / sizeof(float))];
            }
            catch (const std::bad_alloc& e) {
                HIAI_ENGINE_LOG(HIAI_IDE_ERROR, "[CaffeInferenceEngine_1] outputData alloc fails.");
                releaseInputBuffer();
                return HIAI_ERROR;
            }
            ConvertU1ToF1(inputBuffer1, outputData, nnInputSize); //caffe need data type float
            Blob<float>* inputLayer = net_->input_blobs()[0];
            float *tmp = outputData; //for static check
            inputLayer->set_cpu_data(tmp);

            //process multi port input, for instance:fasterrcnn im_info
            setCPUData();

            //start forward inference
            PyGILState_STATE state = PyGILState_Ensure(); //get GIL before call python api, caffe may call layer implemented by python
            net_->Forward();
            PyGILState_Release(state); //release GIL after call python api

            for (int i = 0; i < net_->output_blobs().size(); i++) {
                hiai::AINeuralNetworkBuffer *networkBuffer = nullptr;
                try {
                    networkBuffer = new hiai::AINeuralNetworkBuffer();
                }
                catch (const std::bad_alloc& e) {
                    HIAI_ENGINE_LOG(HIAI_IDE_ERROR, "[CaffeInferenceEngine_1] tmp alloc fails.");
                    releaseInputBuffer();
                    return HIAI_ERROR;
                }
                std::shared_ptr<hiai::AINeuralNetworkBuffer> neuralBuffer = std::shared_ptr<hiai::AINeuralNetworkBuffer>(networkBuffer);
                if (NULL == neuralBuffer || NULL == neuralBuffer.get()) {
                    HIAI_ENGINE_LOG(HIAI_IDE_ERROR, "[CaffeInferenceEngine_1]img.data is null, ERROR!");
                    delete [] outputData;
                    outputData = NULL;
                    releaseInputBuffer();
                    return HIAI_ERROR;
                }
                const int outputBlobIndex = net_->output_blob_indices()[i];
                const string& outputName = net_->blob_names()[outputBlobIndex];
                HIAI_ENGINE_LOG(HIAI_IDE_INFO, "[CaffeInferenceEngine_1] outputName[%d]:%s.", i, outputName.c_str());
                neuralBuffer->SetName(outputName);
                void* blobBuffer = malloc(net_->output_blobs()[i]->data()->size());
                if (blobBuffer == NULL) {
                    HIAI_ENGINE_LOG(HIAI_IDE_ERROR, "[CaffeInferenceEngine_1] blob buffer malloc failed!");
                    delete [] outputData;
                    outputData = NULL;
                    releaseInputBuffer();
                    return HIAI_ERROR;
                }

                if(memcpy_s(blobBuffer, net_->output_blobs()[i]->data()->size(), (void*)net_->output_blobs()[i]->cpu_data(), net_->output_blobs()[i]->data()->size())) {
                    HIAI_ENGINE_LOG(HIAI_IDE_ERROR, "[CaffeInferenceEngine_1] ERROR, copy info buffer failed");
                    delete [] outputData;
                    free(blobBuffer);
                    outputData = NULL;
                    blobBuffer = NULL;
                    releaseInputBuffer();
                    return HIAI_ERROR;
                }
                neuralBuffer->SetBuffer(blobBuffer, net_->output_blobs()[i]->data()->size());
                std::shared_ptr<hiai::IAITensor> outData = std::static_pointer_cast<hiai::IAITensor>(neuralBuffer);
                outputDataVec.push_back(outData);
            }

            delete [] outputData;
            outputData = NULL;
        }
    }

    releaseInputBuffer();
    ProcessCaffeResult(outputDataVec); //process caffe forward result(float to int8), and then send data

    //6.0 release sources
    if (NULL != imageHandle_) {
        for (int i = 0; i < imageNumber; i++) {
            imageHandle_->v_img[i].img.data = nullptr;
        }
    }
    imageHandle_ = nullptr;
    tranData_ = nullptr;
    inputDataVec_.clear();
    HIAI_ENGINE_LOG(HIAI_IDE_INFO, "[CaffeInferenceEngine_1] end process!");
    return HIAI_OK;
}

