/**
*
* Copyright(c)<2018>, <Huawei Technologies Co.,Ltd>
*
* @version 1.0
*
* @date 2018-5-21
*/
#ifndef _CAFFE_INFERENCEENGINE_ENGINE_H_
#define _CAFFE_INFERENCEENGINE_ENGINE_H_
#include "hiaiengine/api.h"
#include "hiaiengine/ai_types.h"
#include "hiaiengine/data_type.h"
#include "hiaiengine/engine.h"
#include "hiaiengine/multitype_queue.h"
#include "hiaiengine/data_type_reg.h"
#include "hiaiengine/ai_tensor.h"
#include "BatchImageParaWithScale.h"
#include <caffe/caffe.hpp>
#define INPUT_SIZE 2
#define OUTPUT_SIZE 1

using namespace caffe;
using hiai::Engine;


//multi input port data push data to queue
#define MULTI_INPUT_ARG_PUSH(index) \
    std::shared_ptr<hiai::BatchRawDataBuffer> multiInput##index; \
    inputQue_.PushData(index-1, arg##index) \

//push and pop other port data
#define MULTI_INPUT_ARG_POP_3 \
    MULTI_INPUT_ARG_PUSH(2); \
do { \
    if (!inputQue_.PopAllData(imageHandle_, multiInput2)) { \
        HIAI_ENGINE_LOG(HIAI_IDE_ERROR, "[CaffeInferenceEngine_1] fail to PopAllData"); \
        return HIAI_ERROR; \
    } \
} while (0)

#define MULTI_INPUT_ARG_POP_4 \
    MULTI_INPUT_ARG_PUSH(2); \
    MULTI_INPUT_ARG_PUSH(3); \
do { \
    if (!inputQue_.PopAllData(imageHandle_, multiInput2, multiInput3)) { \
        HIAI_ENGINE_LOG(HIAI_IDE_ERROR, "[CaffeInferenceEngine_1] fail to PopAllData"); \
        return HIAI_ERROR; \
    } \
} while (0)

#define MULTI_INPUT_ARG_POP_5 \
    MULTI_INPUT_ARG_PUSH(2); \
    MULTI_INPUT_ARG_PUSH(3); \
    MULTI_INPUT_ARG_PUSH(4); \
do { \
    if (!inputQue_.PopAllData(imageHandle_, multiInput2, multiInput3, multiInput4)) { \
        HIAI_ENGINE_LOG(HIAI_IDE_ERROR, "[CaffeInferenceEngine_1] fail to PopAllData"); \
        return HIAI_ERROR; \
    } \
} while (0)

#define MULTI_INPUT_ARG_POP_6 \
    MULTI_INPUT_ARG_PUSH(2); \
    MULTI_INPUT_ARG_PUSH(3); \
    MULTI_INPUT_ARG_PUSH(4); \
    MULTI_INPUT_ARG_PUSH(5); \
do { \
    if (!inputQue_.PopAllData(imageHandle_, multiInput2, multiInput3, multiInput4, multiInput5)) { \
        HIAI_ENGINE_LOG(HIAI_IDE_ERROR, "[CaffeInferenceEngine_1] fail to PopAllData"); \
        return HIAI_ERROR; \
    } \
} while (0)

#define MULTI_INPUT_ARG_POP_7 \
    MULTI_INPUT_ARG_PUSH(2); \
    MULTI_INPUT_ARG_PUSH(3); \
    MULTI_INPUT_ARG_PUSH(4); \
    MULTI_INPUT_ARG_PUSH(5); \
    MULTI_INPUT_ARG_PUSH(6); \
do { \
    if (!inputQue_.PopAllData(imageHandle_, multiInput2, multiInput3, multiInput4, multiInput5, \
        multiInput6)) { \
        HIAI_ENGINE_LOG(HIAI_IDE_ERROR, "[CaffeInferenceEngine_1] fail to PopAllData"); \
        return HIAI_ERROR; \
    } \
} while (0)

#define MULTI_INPUT_ARG_POP_8 \
    MULTI_INPUT_ARG_PUSH(2); \
    MULTI_INPUT_ARG_PUSH(3); \
    MULTI_INPUT_ARG_PUSH(4); \
    MULTI_INPUT_ARG_PUSH(5); \
    MULTI_INPUT_ARG_PUSH(6); \
    MULTI_INPUT_ARG_PUSH(7); \
do { \
    if (!inputQue_.PopAllData(imageHandle_, multiInput2, multiInput3, multiInput4, multiInput5, \
        multiInput6, multiInput7)) { \
        HIAI_ENGINE_LOG(HIAI_IDE_ERROR, "[CaffeInferenceEngine_1] fail to PopAllData"); \
        return HIAI_ERROR; \
    } \
} while (0)

#define MULTI_INPUT_ARG_POP_9 \
    MULTI_INPUT_ARG_PUSH(2); \
    MULTI_INPUT_ARG_PUSH(3); \
    MULTI_INPUT_ARG_PUSH(4); \
    MULTI_INPUT_ARG_PUSH(5); \
    MULTI_INPUT_ARG_PUSH(6); \
    MULTI_INPUT_ARG_PUSH(7); \
    MULTI_INPUT_ARG_PUSH(8); \
do { \
    if (!inputQue_.PopAllData(imageHandle_, multiInput2, multiInput3, multiInput4, multiInput5, \
        multiInput6, multiInput7, multiInput8)) { \
        HIAI_ENGINE_LOG(HIAI_IDE_ERROR, "[CaffeInferenceEngine_1] fail to PopAllData"); \
        return HIAI_ERROR; \
    } \
} while (0)

#define MULTI_INPUT_ARG_POP_10 \
    MULTI_INPUT_ARG_PUSH(2); \
    MULTI_INPUT_ARG_PUSH(3); \
    MULTI_INPUT_ARG_PUSH(4); \
    MULTI_INPUT_ARG_PUSH(5); \
    MULTI_INPUT_ARG_PUSH(6); \
    MULTI_INPUT_ARG_PUSH(7); \
    MULTI_INPUT_ARG_PUSH(8); \
    MULTI_INPUT_ARG_PUSH(9); \
do { \
    if (!inputQue_.PopAllData(imageHandle_, multiInput2, multiInput3, multiInput4, multiInput5, \
        multiInput6, multiInput7, multiInput8, multiInput9)) { \
        HIAI_ENGINE_LOG(HIAI_IDE_ERROR, "[CaffeInferenceEngine_1] fail to PopAllData"); \
        return HIAI_ERROR; \
    } \
} while (0)

#define DEFINE_MULTI_INPUT_ARGS_POP(index) MULTI_INPUT_ARG_POP_##index


//put multi port data(IAITensor format) to vector
#define MULTI_INPUT_ARG(index) \
do { \
    std::shared_ptr<hiai::AINeuralNetworkBuffer> neuralBuffer##index = \
        std::make_shared<hiai::AINeuralNetworkBuffer>(); \
    neuralBuffer##index->SetBuffer((void*)(inputBuffer##index), \
        (uint32_t)(inputBuffer##index##Size), false); \
    std::shared_ptr<hiai::IAITensor> inputData##index = \
        std::static_pointer_cast<hiai::IAITensor>(neuralBuffer##index); \
    inputDataVec_.push_back(inputData##index); \
} while (0)

#define MULTI_INPUT_ARGS_2 \
    MULTI_INPUT_ARG(1);

#define MULTI_INPUT_ARGS_3 \
    MULTI_INPUT_ARG(1); \
    MULTI_INPUT_ARG(2);

#define MULTI_INPUT_ARGS_4 \
    MULTI_INPUT_ARG(1); \
    MULTI_INPUT_ARG(2); \
    MULTI_INPUT_ARG(3);

#define MULTI_INPUT_ARGS_5 \
    MULTI_INPUT_ARG(1); \
    MULTI_INPUT_ARG(2); \
    MULTI_INPUT_ARG(3); \
    MULTI_INPUT_ARG(4);

#define MULTI_INPUT_ARGS_6 \
    MULTI_INPUT_ARG(1); \
    MULTI_INPUT_ARG(2); \
    MULTI_INPUT_ARG(3); \
    MULTI_INPUT_ARG(4); \
    MULTI_INPUT_ARG(5);

#define MULTI_INPUT_ARGS_7 \
    MULTI_INPUT_ARG(1); \
    MULTI_INPUT_ARG(2); \
    MULTI_INPUT_ARG(3); \
    MULTI_INPUT_ARG(4); \
    MULTI_INPUT_ARG(5); \
    MULTI_INPUT_ARG(6);

#define MULTI_INPUT_ARGS_8 \
    MULTI_INPUT_ARG(1); \
    MULTI_INPUT_ARG(2); \
    MULTI_INPUT_ARG(3); \
    MULTI_INPUT_ARG(4); \
    MULTI_INPUT_ARG(5); \
    MULTI_INPUT_ARG(6); \
    MULTI_INPUT_ARG(7);

#define MULTI_INPUT_ARGS_9 \
    MULTI_INPUT_ARG(1); \
    MULTI_INPUT_ARG(2); \
    MULTI_INPUT_ARG(3); \
    MULTI_INPUT_ARG(4); \
    MULTI_INPUT_ARG(5); \
    MULTI_INPUT_ARG(6); \
    MULTI_INPUT_ARG(8);

#define MULTI_INPUT_ARGS_10 \
    MULTI_INPUT_ARG(1); \
    MULTI_INPUT_ARG(2); \
    MULTI_INPUT_ARG(3); \
    MULTI_INPUT_ARG(4); \
    MULTI_INPUT_ARG(5); \
    MULTI_INPUT_ARG(6); \
    MULTI_INPUT_ARG(8); \
    MULTI_INPUT_ARG(9);

#define DEFINE_MULTI_INPUT_ARGS(index) MULTI_INPUT_ARGS_##index


//define release inputBuffer
#define RELEASE_MULTI_INPUT_BUFFER(index) \
do { \
    HIAI_ENGINE_LOG(HIAI_IDE_INFO, "[CaffeInferenceEngine_1] release inputBuffer%d memory", index); \
    if (NULL != inputBuffer##index) { \
        delete [] inputBuffer##index; \
        inputBuffer##index = NULL; \
    } \
} while (0)

#define RELEASE_MULTI_INPUT_BUFFERS_2 \
    RELEASE_MULTI_INPUT_BUFFER(1);

#define RELEASE_MULTI_INPUT_BUFFERS_3 \
    RELEASE_MULTI_INPUT_BUFFER(1); \
    RELEASE_MULTI_INPUT_BUFFER(2);

#define RELEASE_MULTI_INPUT_BUFFERS_4 \
    RELEASE_MULTI_INPUT_BUFFER(1); \
    RELEASE_MULTI_INPUT_BUFFER(2); \
    RELEASE_MULTI_INPUT_BUFFER(3);

#define RELEASE_MULTI_INPUT_BUFFERS_5 \
    RELEASE_MULTI_INPUT_BUFFER(1); \
    RELEASE_MULTI_INPUT_BUFFER(2); \
    RELEASE_MULTI_INPUT_BUFFER(3); \
    RELEASE_MULTI_INPUT_BUFFER(4);

#define RELEASE_MULTI_INPUT_BUFFERS_6 \
    RELEASE_MULTI_INPUT_BUFFER(1); \
    RELEASE_MULTI_INPUT_BUFFER(2); \
    RELEASE_MULTI_INPUT_BUFFER(3); \
    RELEASE_MULTI_INPUT_BUFFER(4); \
    RELEASE_MULTI_INPUT_BUFFER(5);

#define RELEASE_MULTI_INPUT_BUFFERS_7 \
    RELEASE_MULTI_INPUT_BUFFER(1); \
    RELEASE_MULTI_INPUT_BUFFER(2); \
    RELEASE_MULTI_INPUT_BUFFER(3); \
    RELEASE_MULTI_INPUT_BUFFER(4); \
    RELEASE_MULTI_INPUT_BUFFER(5); \
    RELEASE_MULTI_INPUT_BUFFER(6);

#define RELEASE_MULTI_INPUT_BUFFERS_8 \
    RELEASE_MULTI_INPUT_BUFFER(1); \
    RELEASE_MULTI_INPUT_BUFFER(2); \
    RELEASE_MULTI_INPUT_BUFFER(3); \
    RELEASE_MULTI_INPUT_BUFFER(4); \
    RELEASE_MULTI_INPUT_BUFFER(5); \
    RELEASE_MULTI_INPUT_BUFFER(6); \
    RELEASE_MULTI_INPUT_BUFFER(7);

#define RELEASE_MULTI_INPUT_BUFFERS_9 \
    RELEASE_MULTI_INPUT_BUFFER(1); \
    RELEASE_MULTI_INPUT_BUFFER(2); \
    RELEASE_MULTI_INPUT_BUFFER(3); \
    RELEASE_MULTI_INPUT_BUFFER(4); \
    RELEASE_MULTI_INPUT_BUFFER(5); \
    RELEASE_MULTI_INPUT_BUFFER(6); \
    RELEASE_MULTI_INPUT_BUFFER(8);

#define RELEASE_MULTI_INPUT_BUFFERS_10 \
    RELEASE_MULTI_INPUT_BUFFER(1); \
    RELEASE_MULTI_INPUT_BUFFER(2); \
    RELEASE_MULTI_INPUT_BUFFER(3); \
    RELEASE_MULTI_INPUT_BUFFER(4); \
    RELEASE_MULTI_INPUT_BUFFER(5); \
    RELEASE_MULTI_INPUT_BUFFER(6); \
    RELEASE_MULTI_INPUT_BUFFER(8); \
    RELEASE_MULTI_INPUT_BUFFER(9);

#define RELEASE_MULTI_INPUT_BUFFERS(index) RELEASE_MULTI_INPUT_BUFFERS_##index


//set caffe cpu data for multi input
#define CAFFE_SET_CPU_DATA(index)  \
do { \
    if (index <= net_->num_inputs()) { \
        HIAI_ENGINE_LOG(HIAI_IDE_INFO, "[CaffeInferenceEngine_1] input_blobs[%d] set cpu data", index); \
        net_->input_blobs()[index - 1]->set_cpu_data((float *)inputBuffer##index); \
    } \
} while (0)

#define CAFFE_SET_CPU_DATAS_2 \
    CAFFE_SET_CPU_DATA(2);

#define CAFFE_SET_CPU_DATAS_3 \
    CAFFE_SET_CPU_DATA(2); \
    CAFFE_SET_CPU_DATA(3);

#define CAFFE_SET_CPU_DATAS_4 \
    CAFFE_SET_CPU_DATA(2); \
    CAFFE_SET_CPU_DATA(3); \
    CAFFE_SET_CPU_DATA(4);

#define CAFFE_SET_CPU_DATAS_5 \
    CAFFE_SET_CPU_DATA(2); \
    CAFFE_SET_CPU_DATA(3); \
    CAFFE_SET_CPU_DATA(4); \
    CAFFE_SET_CPU_DATA(5);

#define CAFFE_SET_CPU_DATAS_6 \
    CAFFE_SET_CPU_DATA(2); \
    CAFFE_SET_CPU_DATA(3); \
    CAFFE_SET_CPU_DATA(4); \
    CAFFE_SET_CPU_DATA(5); \
    CAFFE_SET_CPU_DATA(6);

#define CAFFE_SET_CPU_DATAS_7 \
    CAFFE_SET_CPU_DATA(2); \
    CAFFE_SET_CPU_DATA(3); \
    CAFFE_SET_CPU_DATA(4); \
    CAFFE_SET_CPU_DATA(5); \
    CAFFE_SET_CPU_DATA(6); \
    CAFFE_SET_CPU_DATA(7);

#define CAFFE_SET_CPU_DATAS_8 \
    CAFFE_SET_CPU_DATA(2); \
    CAFFE_SET_CPU_DATA(3); \
    CAFFE_SET_CPU_DATA(4); \
    CAFFE_SET_CPU_DATA(5); \
    CAFFE_SET_CPU_DATA(6); \
    CAFFE_SET_CPU_DATA(7); \
    CAFFE_SET_CPU_DATA(8);

#define CAFFE_SET_CPU_DATAS_9 \
    CAFFE_SET_CPU_DATA(2); \
    CAFFE_SET_CPU_DATA(3); \
    CAFFE_SET_CPU_DATA(4); \
    CAFFE_SET_CPU_DATA(5); \
    CAFFE_SET_CPU_DATA(6); \
    CAFFE_SET_CPU_DATA(8); \
    CAFFE_SET_CPU_DATA(9);

#define CAFFE_SET_CPU_DATAS_10 \
    CAFFE_SET_CPU_DATA(2); \
    CAFFE_SET_CPU_DATA(3); \
    CAFFE_SET_CPU_DATA(4); \
    CAFFE_SET_CPU_DATA(5); \
    CAFFE_SET_CPU_DATA(6); \
    CAFFE_SET_CPU_DATA(8); \
    CAFFE_SET_CPU_DATA(9); \
    CAFFE_SET_CPU_DATA(10);

#define CAFFE_SET_CPU_DATAS(index) CAFFE_SET_CPU_DATAS_##index


typedef union {
    float f;
    unsigned char data[4];
} FloatData;

typedef struct CaffeInferConfigInfoT {
    const char *modelPath_;
    const char *weightPath_;
    std::string dumpPath_;
    std::string dumpList_;

    CaffeInferConfigInfoT() {
        modelPath_ = nullptr;
        weightPath_ = nullptr;
        dumpPath_ = "";
        dumpList_ = "";
    }
}CaffeInferConfigInfo;

class CaffeInferenceEngine_1 : public Engine {
#define CAFFE_DUMP_PATH_LEN 256
public:
    CaffeInferenceEngine_1() : inputQue_(INPUT_SIZE - 1), tranData_(nullptr), dataInput_(nullptr), imageHandle_(nullptr), batchSize_(1) {}
    ~CaffeInferenceEngine_1() {}
    HIAI_StatusT Init(const hiai::AIConfig& config, const  std::vector<hiai::AIModelDescription>& modelDesc);
    HIAI_DEFINE_PROCESS(INPUT_SIZE, OUTPUT_SIZE);

private:
    void HandleExceptions(const std::string errorMsg);
    void HandleModelBatchFailure(const int batchBegin);
    void PrepareData(const std::shared_ptr<void> &data);
    HIAI_StatusT PrepareInfoInput(uint8_t *inputBuffer, const int batchBegin, const std::shared_ptr<hiai::BatchRawDataBuffer> &multiInput);
    HIAI_StatusT SendSentinelImage();
    void CaffeDumpProcess(const uint32_t imageFrameID, const uint8_t *buffer1,
        const int buffer1Size, const uint8_t *buffer2 = nullptr, const int buffer2Size = 0);
    bool StorageCaffeData(const unsigned char *inputData, const uint32_t size, const uint32_t imageFrameID, const char *fileName);
    void DumpCaffeLayer(const uint32_t imageFrameID, std::string file, std::string imgInfoFile = "");
    HIAI_StatusT PrepareInputBuffer(uint8_t *inputBuffer, const int imageNumber, const int batchBegin, const int imageSize);
    HIAI_StatusT CheckCaffeParam();
    HIAI_StatusT ProcessCaffeResult(std::vector<std::shared_ptr<hiai::IAITensor>> &outputDataVec);

private:
    boost::shared_ptr<Net<float>> net_;
    hiai::MultiTypeQueue inputQue_;
    CaffeInferConfigInfo caffeCfgInfo_;
    char caffeDumpName_[CAFFE_DUMP_PATH_LEN];
    std::shared_ptr<BatchImageParaWithScaleT> dataInput_;
    std::shared_ptr<BatchImageParaWithScaleT> imageHandle_;
    std::shared_ptr<EngineTransT> tranData_;
    std::vector<std::shared_ptr<hiai::IAITensor>> inputDataVec_;
    unsigned int batchSize_; //batch_size from prototxt
};

#endif

