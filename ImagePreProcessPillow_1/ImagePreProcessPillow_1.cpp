/**
* @file ImagePreProcessPillow_1.cpp
*
* Copyright(c)<2018>, <Huawei Technologies Co.,Ltd>
*
* @version 1.0
*
* @date 2018-4-25
*/

#include "ImagePreProcessPillow_1.h"
#include "hiaiengine/log.h"
#include "hiaiengine/ai_types.h"
#include "hiaiengine/data_type_reg.h"
#include <sstream>
#include <iostream>
#include <string>

static long int crv_tab[256];
static long int cbu_tab[256];
static long int cgu_tab[256];
static long int cgv_tab[256];
static long int tab_76309[256];
static unsigned char clp[1024];

using namespace std;


ImagePreProcessPillow_1::~ImagePreProcessPillow_1()
{
    HIAI_ENGINE_LOG(HIAI_IDE_INFO, "[ImagePreProcessPillow_1] ImagePreProcessPillow_1 Engine Destory.");
}

HIAI_StatusT ImagePreProcessPillow_1::Init(const hiai::AIConfig& config, const std::vector<hiai::AIModelDescription>& model_desc)
{
    m_pillow_config_ = std::make_shared<PillowConfig>();
    if (NULL == m_pillow_config_ || NULL == m_pillow_config_.get()) {
        HIAI_ENGINE_LOG(HIAI_IDE_ERROR, "[ImagePreProcessPillow_1] Init: m_pillow_config_ alloc fail");
        return HIAI_ERROR;
    }
    stringstream ss;
    for (int index = 0; index < config.items_size(); ++index) {
        const ::hiai::AIConfigItem& item = config.items(index);
        std::string name = item.name();
        ss << item.value();
        if ("point_x" == name) {
           ss >> (*m_pillow_config_).point_x;
        } else if ("point_y" == name) {
            ss >> (*m_pillow_config_).point_y;
        } else if ("crop_width" == name) {
            ss >> (*m_pillow_config_).crop_width;
        } else if ("crop_height" == name) {
            ss >> (*m_pillow_config_).crop_height;
        } else if ("self_crop" == name) {
            ss >> (*m_pillow_config_).self_crop;
        } else if ("dump_value" == name) {
            ss >> (*m_pillow_config_).dump_value;
        } else if ("resize_height" == name) {
            ss >> (*m_pillow_config_).resize_height;
        } else if ("resize_width" == name) {
            ss >> (*m_pillow_config_).resize_width;
        } else if ("crop_before_resize" == name) {
            ss >> (*m_pillow_config_).crop_before_resize;
        } else if ("mean_of_B" == name) {
            ss >> (*m_pillow_config_).mean_of_B;
        } else if ("mean_of_G" == name) {
            ss >> (*m_pillow_config_).mean_of_G;
        } else if ("mean_of_R" == name) {
            ss >> (*m_pillow_config_).mean_of_R;
        } else if ("scale_value" == name) {
            ss >> (*m_pillow_config_).scale_value;
        }
        ss.clear();
    }

    return HIAI_OK;
}

vector<char> ImagePreProcessPillow_1::listTupleToVector_Char(PyObject *pObj)
{
    vector<char> data;
    if (PyList_Check(pObj)) {
        for (Py_ssize_t i = 3; i < PyList_Size(pObj); i++) {
            PyObject *value = PyList_GetItem(pObj, i);
            double d = PyFloat_AsDouble(value);
            data.push_back(d);
        }
    } else {
        HIAI_ENGINE_LOG(HIAI_IDE_ERROR, "[ImagePreProcessPillow_1] Passed PyObject pointer was not a list or tuple!");
    }
    return data;
}

int ImagePreProcessPillow_1::__handle_img(ImageData<u_int8_t> &img)
{
    HIAI_ENGINE_LOG(HIAI_IDE_INFO, "[ImagePreProcessPillow_1] __handle_img process!");
    unsigned char *yuvbuffer = nullptr;
    u_int8_t *rgbbuffer = nullptr;
    char *img_data = nullptr;

    if ((ImageTypeT)img.format == IMAGE_TYPE_NV12) {
        yuvbuffer = (unsigned char *)(img.data.get());
        if (img.width <= 0 || img.height <= 0) {
            HIAI_ENGINE_LOG(HIAI_IDE_ERROR, "[ImagePreProcessPillow_1] __handle_img: image width or height is invalid!");
            return HIAI_ERROR;
        }

        try {
            rgbbuffer = new u_int8_t[img.width * img.height * 3];
        }
        catch (const std::bad_alloc& e) {
            HIAI_ENGINE_LOG(HIAI_IDE_ERROR, "[ImagePreProcessPillow_1] __handle_img: new rgbbuffer failed");
            return HIAI_ERROR;
        }
        yuv420sp_to_rgb24(yuvbuffer, rgbbuffer, img.width, img.height);
        img_data = (char *)(rgbbuffer);
        img.size = img.width * img.height * 3;
    } else {
        img_data = (char *)(img.data.get());
    }

    if (!PyEval_ThreadsInitialized())
        PyEval_InitThreads();
    PyThreadStateLock pyLock; //get GIL before call python api
    string curDirCmd = string("sys.path.append(os.curdir + '/..')");
    PyRun_SimpleString("import sys");
    PyRun_SimpleString("import os");
    PyRun_SimpleString(curDirCmd.c_str());

    PyObject *pModule = nullptr, *pFunc = nullptr;
    PyObject *pArgs = nullptr, *pValue = nullptr;
    /* import */
    pModule = PyImport_Import(PyString_FromString("ImagePreProcessPillow_1.ImagePreProcessPillow_1"));
    if (nullptr == pModule) {
        HIAI_ENGINE_LOG(HIAI_IDE_ERROR, "[ImagePreProcessPillow_1] [ERROR] Python get module failed.");
        if (nullptr != rgbbuffer) {
            delete [] rgbbuffer;
            rgbbuffer = nullptr;
        }
        return HIAI_ERROR;
    }

    pFunc = PyObject_GetAttrString(pModule, "do_preprocess");
    if (nullptr == pFunc || !PyCallable_Check(pFunc)) {
        HIAI_ENGINE_LOG(HIAI_IDE_ERROR, "[ImagePreProcessPillow_1] [ERROR] Can't find funftion (test_add)");
        if (nullptr != rgbbuffer) {
            delete [] rgbbuffer;
            rgbbuffer = nullptr;
        }
        return HIAI_ERROR;
    }

    // if crop_width and crop_height is too large, we need to fix them.
    int fixedWidth = m_pillow_config_->crop_width;
    int fixedHeight = m_pillow_config_->crop_height;
    if (__need_crop()) {
        if ((m_pillow_config_->point_x + m_pillow_config_->crop_width) > img.width) {
            fixedWidth = img.width - m_pillow_config_->point_x;
        }
        if ((m_pillow_config_->point_y + m_pillow_config_->crop_height) > img.height) {
            fixedHeight = img.height - m_pillow_config_->point_y;
        }
    }

    PyObject *buildBytes = Py_BuildValue("s#", img_data, img.size);
    PyObject *buildSize = Py_BuildValue("i", img.size);
    PyObject *buildFormat = Py_BuildValue("i", img.format);
    PyObject *buildWidth_Height = Py_BuildValue("(i,i)", img.width, img.height);
    PyObject *buildCrop = Py_BuildValue("(i,i,i,i)", (*m_pillow_config_).point_x, (*m_pillow_config_).point_y,
        fixedWidth, fixedHeight);
    PyObject *buildResize = Py_BuildValue("(i,i)", int((*m_pillow_config_).resize_width), int((*m_pillow_config_).resize_height));
    PyObject *buildMeanValue = Py_BuildValue("(i,i,i)", (*m_pillow_config_).mean_of_B, (*m_pillow_config_).mean_of_G,
        (*m_pillow_config_).mean_of_R);
    PyObject *buildScale = Py_BuildValue("i", int((*m_pillow_config_).scale_value));
    PyObject *buildDump = Py_BuildValue("i", int((*m_pillow_config_).dump_value));
    PyObject *buildIndex = Py_BuildValue("i", int(m_imageFrameID));
    PyObject *buildOrder = Py_BuildValue("i", int(m_orderInFrame));
    pArgs = PyTuple_New(11);
    PyTuple_SetItem(pArgs, 0, buildBytes);
    PyTuple_SetItem(pArgs, 1, buildSize);
    PyTuple_SetItem(pArgs, 2, buildFormat);
    PyTuple_SetItem(pArgs, 3, buildWidth_Height);
    PyTuple_SetItem(pArgs, 4, buildCrop);
    PyTuple_SetItem(pArgs, 5, buildResize);
    PyTuple_SetItem(pArgs, 6, buildMeanValue);
    PyTuple_SetItem(pArgs, 7, buildScale);
    PyTuple_SetItem(pArgs, 8, buildDump);
    PyTuple_SetItem(pArgs, 9, buildIndex);
    PyTuple_SetItem(pArgs, 10, buildOrder);

    pValue = PyObject_CallObject(pFunc, pArgs);
    if (nullptr == pValue || !PyList_Check(pValue)) {
        HIAI_ENGINE_LOG(HIAI_IDE_ERROR, "[ImagePreProcessPillow_1] Passed PyObject pointer was not a list or tuple!");
        if (nullptr != rgbbuffer) {
            delete [] rgbbuffer;
            rgbbuffer = NULL;
        }
        return HIAI_ERROR;
    }

    //release IMAGE_TYPE_NV12 memory
    if (nullptr != rgbbuffer) {
        delete [] rgbbuffer;
        rgbbuffer = NULL;
    }

    PyObject *value = PyList_GetItem(pValue, 0);
    int new_width = int(PyFloat_AsDouble(value));

    value = PyList_GetItem(pValue, 1);
    int new_height = int(PyFloat_AsDouble(value));

    value = PyList_GetItem(pValue, 2);
    int new_size = int(PyFloat_AsDouble(value));

    vector<char> v = listTupleToVector_Char(pValue);
    if(v.size() <= 0) {
        HIAI_ENGINE_LOG(HIAI_IDE_ERROR, "[ImagePreProcessPillow_1] __handle_img: v.size is invalid!");
        return HIAI_ERROR;
    }

    uint8_t *tmp = nullptr;
    try {
        tmp = new uint8_t[v.size()];
    }
    catch (const std::bad_alloc& e) {
        HIAI_ENGINE_LOG(HIAI_IDE_ERROR, "[ImagePreProcessPillow_1] failed to allocate buffer for out_buffer");
        return HIAI_ERROR;
    }
    std::shared_ptr<u_int8_t> out_buffer = std::shared_ptr<u_int8_t>(tmp);
    for (unsigned int i = 0; i < v.size(); ++i) {
        out_buffer.get()[i] = v[i];
    }

    std::shared_ptr<NewImageParaT> new_img = std::make_shared<NewImageParaT>();
    if (NULL == new_img || NULL == new_img.get()) {
        HIAI_ENGINE_LOG("[ImagePreProcessPillow_1] __handle_img: new_img alloc fail!");
        if (nullptr != tmp) {
            delete [] tmp;
            tmp = nullptr;
        }
        return HIAI_ERROR;
    }
    new_img->img.width = new_width;
    new_img->img.height = new_height;
    new_img->img.size = new_size;
    new_img->img.channel = img.channel;
    //new_img->img.format = img.format;
    new_img->img.format = (hiai::IMAGEFORMAT)IMAGE_TYPE_RAW;
    new_img->scale_info.scale_width = (1.0*new_img->img.width)/(img.width);
    new_img->scale_info.scale_height = (1.0*new_img->img.height)/(img.height);
    new_img->resize_info.resize_width = m_pillow_config_->resize_width;
    new_img->resize_info.resize_height = m_pillow_config_->resize_height;
    new_img->crop_info.point_x = m_pillow_config_->point_x;
    new_img->crop_info.point_y = m_pillow_config_->point_y;
    new_img->crop_info.crop_width = m_pillow_config_->crop_width;
    new_img->crop_info.crop_height = m_pillow_config_->crop_height;
    new_img->img.data = out_buffer;

    if (NULL == m_pillow_out_ || NULL == m_pillow_out_.get()) {
        m_pillow_out_ = std::make_shared<BatchImageParaWithScaleT>();
        if (NULL == m_pillow_out_ || NULL == m_pillow_out_.get()) {
            if (nullptr != tmp) {
                delete [] tmp;
                tmp = nullptr;
            }
            HIAI_ENGINE_LOG("[ImagePreProcessPillow_1] __handle_img: m_pillow_out_ alloc fail!");
            return HIAI_ERROR;
        }
        m_pillow_out_->b_info = m_pillow_in_->b_info;
        m_pillow_out_->b_info.frame_ID.clear();
    }

    m_pillow_out_->v_img.push_back(*new_img);
    m_pillow_out_->b_info.frame_ID.push_back(m_imageFrameID);

    Py_DECREF(pModule);
    Py_DECREF(pFunc);
    Py_DECREF(pArgs);
    Py_DECREF(pValue);

    return HIAI_OK;
}

int ImagePreProcessPillow_1::__update_crop_para(const Rectangle<Point2D> &rect)
{
    (*m_pillow_config_).point_x = (int)rect.anchor_lt.x;
    (*m_pillow_config_).point_y = (int)rect.anchor_lt.y;
    (*m_pillow_config_).crop_width = (int)(rect.anchor_rb.x - rect.anchor_lt.x);
    (*m_pillow_config_).crop_height = (int)(rect.anchor_rb.y - rect.anchor_lt.y);

    if ((*m_pillow_config_).point_x < 0 || (*m_pillow_config_).point_x % 2 != 0
        || (*m_pillow_config_).point_y < 0 || (*m_pillow_config_).point_y % 2 != 0
        || (*m_pillow_config_).crop_height <= 0 || (*m_pillow_config_).crop_height % 2 != 0
        || (*m_pillow_config_).crop_width <= 0 || (*m_pillow_config_).crop_width % 2 != 0) {
        HIAI_ENGINE_LOG(HIAI_IDE_ERROR, "[ImagePreProcessPillow_1] [Crop_config] Some crop parms are invalid!");
        return HIAI_ERROR;
    }

    return HIAI_OK;
}

bool ImagePreProcessPillow_1::__send_data()
{
    if (NULL == m_pillow_out_ || NULL == m_pillow_out_.get()) {
        HIAI_ENGINE_LOG(HIAI_IDE_ERROR, "[ImagePreProcessPillow_1] Nothing to send!");
        return false;
    }

    if (m_pillow_out_->v_img.size() > 0) {
        HIAI_StatusT hiai_ret = HIAI_OK;
        do {
            hiai_ret = SendData(0, "BatchImageParaWithScaleT", std::static_pointer_cast<void>(m_pillow_out_));
            if (HIAI_QUEUE_FULL == hiai_ret)  {
                HIAI_ENGINE_LOG(HIAI_IDE_INFO, "[ImagePreProcessPillow_1] queue full, sleep 200ms");
                usleep(200000);
            }
        } while (hiai_ret == HIAI_QUEUE_FULL);

        if (HIAI_OK != hiai_ret) {
            HIAI_ENGINE_LOG(HIAI_IDE_ERROR, "[ImagePreProcessPillow_1] SendData failed! error code: %d", hiai_ret);
            return false;
        }
    }
    return true;
}

void ImagePreProcessPillow_1::__clear_data()
{
    for (unsigned int i = 0; i < m_pillow_in_->v_img.size(); i++) {
        m_pillow_in_->v_img[i].img.data = nullptr;
    }
    m_pillow_in_ = NULL;
    m_pillow_out_ = NULL;
    m_pillow_crop_in_ = NULL;
}

bool ImagePreProcessPillow_1::__need_crop()
{
    bool crop = true;
    if (-1 == m_pillow_config_->point_x || -1 == m_pillow_config_->point_y
        || -1 == m_pillow_config_->crop_width || -1 == m_pillow_config_->crop_height) {
        crop = false;
    }
    return crop;
}

int ImagePreProcessPillow_1::__handle_pillow()
{
    if (NULL == m_pillow_in_ || NULL == m_pillow_in_.get()) {
        HIAI_ENGINE_LOG(HIAI_IDE_ERROR, "[ImagePreProcessPillow_1]__hanlde_pillow, input data is null!");
        return HIAI_ERROR;
    }

    HIAI_ENGINE_LOG(HIAI_IDE_INFO, "[ImagePreProcessPillow_1]__hanlde_pillow process");

    int index_crop = 0;
    int i = 0;
    m_orderInFrame = 0;
    for (std::vector<NewImageParaT>::iterator iter = m_pillow_in_->v_img.begin(); iter != m_pillow_in_->v_img.end(); ++iter, i++) {
        m_imageFrameID = m_pillow_in_->b_info.frame_ID[i];
        HIAI_ENGINE_LOG(HIAI_IDE_INFO, "[ImagePreProcessPillow_1]__hanlde_pillow process 1, %d, m_imageFrameID:%d, is_last:%d.",
            (*iter).img.format, m_imageFrameID, m_pillow_in_->b_info.is_last);
        if (NULL != m_pillow_crop_in_ && NULL != m_pillow_crop_in_.get()) {
            if(m_pillow_crop_in_->v_location[index_crop].range.empty()) {
                    HIAI_ENGINE_LOG("[ImagePreProcessPillow_1]v_location[index_crop].range is empty, Continue Next");
                    continue;
            }
            for(std::vector<Rectangle<Point2D>>::iterator iterRect = m_pillow_crop_in_->v_location[index_crop].range.begin(); iterRect != m_pillow_crop_in_->v_location[index_crop].range.end(); ++iterRect) {
                m_orderInFrame++;
                if (HIAI_OK != __update_crop_para(*iterRect) || HIAI_OK != __handle_img((*iter).img)) {
                    HIAI_ENGINE_LOG(HIAI_IDE_WARNING, "[ImagePreProcessPillow_1]Handle One Image Error, Continue Next");
                    continue;
                }
            }
        } else {
            if (HIAI_OK != __handle_img((*iter).img)) {
                HIAI_ENGINE_LOG(HIAI_IDE_WARNING, "[ImagePreProcessPillow_1]Handle One Image Error, Continue Next");
                continue;
            }
        }

        index_crop++;
    }

    return HIAI_OK;
}

void ImagePreProcessPillow_1::init_yuv420p_table()
{
    long int crv = 0, cbu = 0, cgu = 0, cgv = 0;
    int i = 0, ind = 0;
    static int init = 0;

    if (init == 1) {
        return;
    }

    crv = 104597; cbu = 132201;  /* fra matrise i global.h */
    cgu = 25675; cgv = 53279;

    for (i = 0; i < 256; i++) {
        crv_tab[i] = (i-128) * crv;
        cbu_tab[i] = (i-128) * cbu;
        cgu_tab[i] = (i-128) * cgu;
        cgv_tab[i] = (i-128) * cgv;
        tab_76309[i] = 76309*(i-16);
    }

    for (i = 0; i < 384; i++) {
        clp[i] = 0;
    }
    ind = 384;
    for (i = 0;i < 256; i++) {
        clp[ind++] = i;
    }
    ind = 640;
    for (i = 0;i < 384; i++) {
        clp[ind++] = 255;
    }

    init = 1;
}

void ImagePreProcessPillow_1::yuv420sp_to_rgb24(unsigned char *yuvbuffer, unsigned char *rgbbuffer, const int width, const int height)
{
    if(NULL == yuvbuffer || NULL == rgbbuffer) {
        return;
    }

    int y1 = 0, y2 = 0, u = 0, v = 0;
    unsigned char *py1 = nullptr, *py2 = nullptr;
    int i = 0, j = 0, c1 = 0, c2 = 0, c3 = 0, c4 = 0;
    unsigned char *d1 = nullptr, *d2 = nullptr;
    unsigned char *src_u = nullptr;

    src_u = yuvbuffer + width * height;   // u

    py1 = yuvbuffer;   // y
    py2 = py1 + width;
    d1 = rgbbuffer;
    d2 = d1 + 3 * width;

    init_yuv420p_table();

    for (j = 0; j < height; j += 2) {
        for (i = 0; i < width; i += 2) {
            u = *src_u++;
            v = *src_u++;

            c1 = crv_tab[v];
            c2 = cgu_tab[u];
            c3 = cgv_tab[v];
            c4 = cbu_tab[u];

            //up-left
            y1 = tab_76309[*py1++];
            *d1++ = clp[384+((y1 + c1)>>16)];
            *d1++ = clp[384+((y1 - c2 - c3)>>16)];
            *d1++ = clp[384+((y1 + c4)>>16)];

            //down-left
            y2 = tab_76309[*py2++];
            *d2++ = clp[384+((y2 + c1)>>16)];
            *d2++ = clp[384+((y2 - c2 - c3)>>16)];
            *d2++ = clp[384+((y2 + c4)>>16)];

            //up-right
            y1 = tab_76309[*py1++];
            *d1++ = clp[384+((y1 + c1)>>16)];
            *d1++ = clp[384+((y1 - c2 - c3)>>16)];
            *d1++ = clp[384+((y1 + c4)>>16)];

            //down-right
            y2 = tab_76309[*py2++];
            *d2++ = clp[384+((y2 + c1)>>16)];
            *d2++ = clp[384+((y2 - c2 - c3)>>16)];
            *d2++ = clp[384+((y2 + c4)>>16)];
        }
        d1 += 3 * width;
        d2 += 3 * width;
        py1 += width;
        py2 += width;
    }
}

/**
* @ingroup hiaiengine
* @brief HIAI_DEFINE_PROCESS : ImagePreProcessPillow_1
* @[in]:
*
*/
HIAI_IMPL_ENGINE_PROCESS("ImagePreProcessPillow_1", ImagePreProcessPillow_1, INPUT_SIZE)
{
    int err_code = HIAI_OK;
    std::shared_ptr<BatchImageParaWithScaleT> error_handler;

    HIAI_ENGINE_LOG(HIAI_IDE_INFO, "[ImagePreProcessPillow_1] start process!");

    if ((*m_pillow_config_).self_crop != 0) {
      if (NULL == arg0) {
         HIAI_ENGINE_LOG(HIAI_IDE_ERROR, "[ImagePreProcessPillow_1]fail to process invalid message");
         goto ERROR;
      }
      m_pillow_in_ = std::static_pointer_cast<BatchImageParaWithScaleT>(arg0);
      if (NULL == m_pillow_in_ || NULL == m_pillow_in_.get()) {
        HIAI_ENGINE_LOG(HIAI_IDE_ERROR, "[ImagePreProcessPillow_1]fail to process invalid message");
        goto ERROR;
      }
    } else {
        input_que_.PushData(0, arg0);
#if INPUT_SIZE == 2
        input_que_.PushData(1, arg1);
        if (!input_que_.PopAllData(m_pillow_in_, m_pillow_crop_in_)) {
            HIAI_ENGINE_LOG(HIAI_IDE_ERROR, "[ImagePreProcessPillow_1]fail to process invalid message");
            return HIAI_ERROR;
        }
        if (NULL == m_pillow_in_ || NULL == m_pillow_in_.get() || NULL == m_pillow_crop_in_ || NULL == m_pillow_crop_in_.get()) {
            HIAI_ENGINE_LOG(HIAI_IDE_ERROR, "[ImagePreProcessPillow_1]fail to process invalid message");
            goto ERROR;
        }
#else
        if (!input_que_.PopAllData(m_pillow_in_)) {
            HIAI_ENGINE_LOG(HIAI_IDE_ERROR, "[ImagePreProcessPillow_1]fail to process invalid message");
            return HIAI_ERROR;
        }
#endif
   }

    //add sentinel image for showing this data in dataset are all sended, this is last step.
    if (isSentinelImage(m_pillow_in_)) {
        HIAI_ENGINE_LOG(HIAI_IDE_INFO, "[ImagePreProcessPillow_1]sentinel Image, process over.");
        HIAI_StatusT hiai_ret = HIAI_OK;
        do{
            hiai_ret = SendData(0, "BatchImageParaWithScaleT", std::static_pointer_cast<void>(m_pillow_in_));
            if (HIAI_OK != hiai_ret) {
                if (HIAI_ENGINE_NULL_POINTER == hiai_ret || HIAI_HDC_SEND_MSG_ERROR == hiai_ret || HIAI_HDC_SEND_ERROR == hiai_ret
                    || HIAI_GRAPH_SRC_PORT_NOT_EXIST == hiai_ret || HIAI_GRAPH_ENGINE_NOT_EXIST == hiai_ret) {
                    HIAI_ENGINE_LOG(HIAI_IDE_ERROR, "[ImagePreProcessPillow_1] SendData error[%d], break.", hiai_ret);
                    break;
                }
                HIAI_ENGINE_LOG(HIAI_IDE_INFO, "[ImagePreProcessPillow_1] SendData return value[%d] not OK, sleep 200ms", hiai_ret);
                usleep(SEND_DATA_INTERVAL_MS);
            }
        } while (HIAI_OK != hiai_ret);
        return hiai_ret;
    }

    m_pillow_out_ = NULL;
    err_code = __handle_pillow();
    if (HIAI_ERROR == err_code) {
        HIAI_ENGINE_LOG(HIAI_IDE_ERROR, "[ImagePreProcessPillow_1]pillow process error!");
        goto ERROR;
    }

    if(!__send_data()) {
        goto ERROR;
    }

    HIAI_ENGINE_LOG(HIAI_IDE_INFO, "[ImagePreProcessPillow_1] end process!");
    __clear_data();
    return HIAI_OK;

ERROR:
    //send null to next node to avoid blocking when to encounter abnomal situation
    SendData(0, "BatchImageParaWithScaleT", std::static_pointer_cast<void>(error_handler));
    __clear_data();
    return HIAI_ERROR;
}

