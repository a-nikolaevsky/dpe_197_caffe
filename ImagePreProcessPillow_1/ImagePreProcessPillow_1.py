# -*- coding:utf-8 -*-
import gevent.monkey
gevent.monkey.patch_thread()

import os
from PIL import Image
import numpy as np
import cStringIO
import shutil

class ImagePreProcessPillow(object):
    '''
        preprocess image with pillow, containg: resize, crop
    '''
    def __init__(self, buffer,format,width_height):
        if( format==0 ):

            self.img =Image.frombytes('RGB',width_height,buffer)
        else:
            self.img = Image.open(cStringIO.StringIO(buffer))

    def crop(self, left, top, width, height):
        """ crop image to give zone """
        self.img = self.img.crop((left, top, left + width, top + height))

    def resize(self, w, h, alg=Image.ANTIALIAS):
        """ resize the image to w x h with the algorithm given """
        if( w <= 0 or h <= 0 ):
            return None                # Exception in para illeage

        self.img = self.img.resize((w, h), alg)

    def saveImage(self, index, order):
        pillowDir = os.path.dirname(os.path.realpath(__file__))
        projectDir = os.path.dirname(pillowDir)
        start_pos = pillowDir.rfind("/")
        if (start_pos < 0):
            return
        pillowDirName = pillowDir[start_pos+1:]
        saveImageDir = projectDir + "/out/PreProcess/" + pillowDirName
        if not os.path.exists(saveImageDir):
            os.makedirs(saveImageDir, 0700) #folder mode:700
        saveName = saveImageDir + "/" + str(index) + "_" + str(order) + "_preProcess.jpg"
        self.img.save(str(saveName))
        os.chmod(saveName, 0600); #file mode:600

    def run(self, buffer_size, crop_config, resize_config, mean_value, scale, dump, index, order):
        """pre process image"""
        self.img = self.img.convert('RGB')
        if(crop_config != None and crop_config[0] != -1 \
                   and crop_config[1] != -1 and crop_config[2] != -1 \
                   and crop_config[3] != -1):
            self.crop(*crop_config)

        if(resize_config != None and resize_config[0] != 0 and resize_config[1] != 0):
            self.resize(*resize_config)

        buffer = np.array(self.img).astype(np.float32).copy()     # RGB

        dataTmp = buffer.copy()
        (dataTmp[:, :, 0], dataTmp[:, :, 2]) = (buffer[:, :, 2], buffer[:, :, 0])
        buffer = dataTmp

        factor = scale / 255.0
        buffer[:, :, 0]  *= factor
        buffer[:, :, 1]  *= factor
        buffer[:, :, 2]  *= factor

        buffer[:, :, 0] -= mean_value[0]
        buffer[:, :, 1] -= mean_value[1]
        buffer[:, :, 2] -= mean_value[2]

        # current HWC -> transform to CHW
        buffer = np.transpose(buffer, (2, 0, 1))
        buffer = np.reshape(buffer, (1, ) + buffer.shape)           # NCHW

        if int(dump) == 1:
            self.saveImage(int(index), int(order))

        bytes = buffer.tobytes()
        len_bytes = len(bytes)

        result = [0 for i in xrange(len_bytes + 3)]

        # store width/height/len of image
        result[0] = self.img.width
        result[1] = self.img.height
        result[2] = len_bytes
        for i in xrange(3, len_bytes + 3):
            result[i] = ord(bytes[i - 3])

        return result

def do_preprocess(buffer, buffer_size, format, width_height, crop_config, resize_config, mean_value, scale, dump, index, order):
    ipp = ImagePreProcessPillow(buffer,format,width_height)
    return ipp.run(buffer_size, crop_config, resize_config, mean_value, scale, dump, index, order)
