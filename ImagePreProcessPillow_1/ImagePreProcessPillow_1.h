/**
* @file ImagePreProcessPillow_1.h
*
* Copyright(c)<2018>, <Huawei Technologies Co.,Ltd>
*
* @version 1.0
*
* @date 2018-4-25
*/


#ifndef ImagePreProcessPillow_1_H_
#define ImagePreProcessPillow_1_H_

#include <Python.h>
#include <vector>

#include "hiaiengine/engine.h"
#include "hiaiengine/data_type_reg.h"
#include "hiaiengine/data_type.h"
#include "BatchImageParaWithScale.h"
#include "hiaiengine/multitype_queue.h"

#define INPUT_SIZE 1
#define OUTPUT_SIZE 1

//python C/C++ api Global Interpreter Lock(GIL)
class PyThreadStateLock
{
public:
    PyThreadStateLock(void)
    {
      state = PyGILState_Ensure(); //get GIL
    }
    ~PyThreadStateLock(void)
    {
      PyGILState_Release(state); //release GIL
    }

private:
    PyGILState_STATE state;
};

using hiai::Engine;
using hiai::AIConfig;
using hiai::AIModelDescription;
using hiai::ImageData;
using hiai::BatchInfo;
using hiai::BatchImagePara;
using hiai::BatchDetectedObjectPara;
using hiai::Rectangle;
using hiai::Point2D;

typedef struct PillowConfig{
    int point_x;            
    int point_y;            
    int crop_width;         
    int crop_height;       
    int self_crop;          
    int dump_value;         
    float resize_width;
    float resize_height;

    bool crop_before_resize;

    int mean_of_B;
    int mean_of_G;
    int mean_of_R;
    int scale_value;

    PillowConfig()
    {
        point_x = -1;
        point_y = -1;
        crop_width = -1;
        crop_height = -1;
        self_crop = 1;
        dump_value = 0;
        resize_width = 0;
        resize_height = 0;
        crop_before_resize = true;
        mean_of_B = 104;
        mean_of_G = 117;
        mean_of_R = 123;
        scale_value = 1;
    }
}PillowConfig;
    
// Define ImagePreProcessPillow_1 
class ImagePreProcessPillow_1 : public Engine {

public:
    ImagePreProcessPillow_1():
        input_que_(INPUT_SIZE), m_pillow_config_(NULL), m_pillow_out_(NULL),
        m_pillow_in_(NULL), m_pillow_crop_in_(NULL), m_imageFrameID(0),
        m_orderInFrame(0){}
    ~ImagePreProcessPillow_1();
    HIAI_StatusT Init(const AIConfig& config,
        const std::vector<AIModelDescription>& model_desc);
    
    /**
    * @ingroup hiaiengine
    * @brief HIAI_DEFINE_PROCESS : 
    * @[in]: 
    */
    HIAI_DEFINE_PROCESS(INPUT_SIZE, OUTPUT_SIZE);

private:
    bool __need_crop();
    void __clear_data();
    int __handle_img(ImageData<u_int8_t> &img);
    bool __send_data();
    vector<char> listTupleToVector_Char(PyObject *pObj);
    int __handle_pillow();
    void yuv420sp_to_rgb24(unsigned char *yuvbuffer, unsigned char *rgbbuffer, const int width, const int height);
    void init_yuv420p_table();
    int __update_crop_para(const Rectangle<Point2D> &rect);
    
private:
    hiai::MultiTypeQueue input_que_;
    std::shared_ptr<PillowConfig> m_pillow_config_;
    std::shared_ptr<BatchImageParaWithScaleT> m_pillow_out_;
    std::shared_ptr<BatchImageParaWithScaleT> m_pillow_in_;
    std::shared_ptr<BatchDetectedObjectPara<Rectangle<Point2D>, float>> m_pillow_crop_in_;
    int m_imageFrameID;
    int m_orderInFrame; //multi net for mark order from one image cutout
};

#endif //ImagePreProcessPillow_1_H_

